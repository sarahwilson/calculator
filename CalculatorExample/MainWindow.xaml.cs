﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;


namespace CalculatorExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Calculator src = new Calculator { CurrentExpression = "0"};

        public MainWindow()
        {
            InitializeComponent();
            DataContext = src;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (src.CurrentExpression == "0")
            {
                Clear();
            }

            var button = sender as Button;
            bool buttonOperator = (bool)button.GetValue(AttachedProperty.IsOperatorProperty);
            char value = char.Parse(button.Content.ToString());

            src.ExpressionCharacters.Add(new ExpressionCharacter { IsOperator = buttonOperator, Character = value });

        }

        private void Equals_Click(object sender, RoutedEventArgs e)
        {
            NCalc.Expression ex = new NCalc.Expression(src.CurrentExpression);
            src.PreviousExpression = src.CurrentExpression;

            double result = 0;
            double.TryParse(ex.Evaluate().ToString(), out result);
            src.CurrentExpression = result.ToString();
        }

        private void Clear()
        {
            src.CurrentExpression = "";
        }

        private void ClearDisplay(object sender, RoutedEventArgs e)
        {
            src.CurrentExpression = "";
            src.PreviousExpression = "";
            src.ExpressionCharacters.Clear();
        }

        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            if (src.CurrentExpression.Length > 0)
            {
                src.CurrentExpression = src.CurrentExpression.Remove(src.CurrentExpression.Length - 1);
            }
        }

        private void Clear_Entry_Click(object sender, RoutedEventArgs e)
        {
            var list = src.ExpressionCharacters;
            var lastChar = list[list.Count - 1];
            
            while (list.Count > 0 && list[list.Count -1].IsOperator != true)
            {
                list.Remove(list[list.Count - 1]);
            }
            
        }

        private void Percentage_Click(object sender, RoutedEventArgs e)
        {
            // int percentage = (int)Math.Round((double)(100 * numb) / total); or (56.00 / 100)*25
        }

        private void SquareRoot_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            if (button.Content.ToString() == "√")
            {
                var val = double.Parse(src.CurrentExpression);
                var result = Math.Round(Math.Sqrt(val), 6);
                src.CurrentExpression = result.ToString();
            }

        }

        private void PlusMinus_Click(object sender, RoutedEventArgs e)
        {
            var list = src.ExpressionCharacters;
                //src.CurrentExpression = (double.Parse(src.CurrentExpression) * -1).ToString();
        }
    }
}
