﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorExample
{
    public class ExpressionCharacter
    {
        public bool IsOperator { get; set; }
        public char Character { get; set; }
    }
}
