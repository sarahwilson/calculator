﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CalculatorExample
{
    public class Calculator : INotifyPropertyChanged
    {
        private string _currentExpression;
        private string _previousExpression;
        private ObservableCollection<ExpressionCharacter> _expressionCharacters;

        public Calculator ()
        {
            ExpressionCharacters = new ObservableCollection<ExpressionCharacter>();
        }


        public ObservableCollection<ExpressionCharacter> ExpressionCharacters
        {
            get
            {
                return _expressionCharacters;
            }
            set
            {
                _expressionCharacters = value;
                _expressionCharacters.CollectionChanged += ExpressionCharactersChanged;
            }
        }

        public void BuildExpressionFromList()
        {
            var builder = new StringBuilder();
            foreach (var item in _expressionCharacters)
            {
                builder.Append(item.Character);
            }
            CurrentExpression = builder.ToString();
        }

        private void ExpressionCharactersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            BuildExpressionFromList();
        }

        public string CurrentExpression
        {
            get { return _currentExpression; }
            set
            {
                if(value != _currentExpression)
                {
                    _currentExpression = value;
                    OnPropertyChanged("CurrentExpression");
                }
            }
        }

        public string PreviousExpression
        {
            get { return _previousExpression; } set {
                if (value != _previousExpression)
                {
                    _previousExpression = value;
                    OnPropertyChanged("PreviousExpression");
                }
            } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
