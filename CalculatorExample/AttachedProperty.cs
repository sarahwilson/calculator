﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CalculatorExample
{
    public class AttachedProperty : FrameworkElement
    {
        public static bool GetIsOperator(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsOperatorProperty);
        }

        public static void SetIsOperator(DependencyObject obj, bool value)
        {
            obj.SetValue(IsOperatorProperty, value);
        }

        public static readonly DependencyProperty IsOperatorProperty =
            DependencyProperty.RegisterAttached("IsOperator",
                typeof(bool), typeof(AttachedProperty));


    }

}
